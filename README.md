# Stampy

A TypeScript library that implements a function `Stampy`.

Result: a template format specified by a simple JSON structure.

Namesake: [this feisty feline](https://www.youtube.com/watch?v=xe5YWvvn4d4).

## Example

```typescript
import { Stampy } from "https://gitlab.com/jane314/stampy/-/raw/main/Stampy.ts";

console.log(
  Stampy(
    // Input #1: the template (a JS object).
    [
      "My name is: ",
      { "name": "name" },
    ],
    // Input #2: the variable inputs (a JS object)
    {
      name: "Jane",
      age: "26",
    },
  ),
);
```

Run the following in [Deno](https://deno.land/) and you will see the output is:

```bash
My name is: Jane
```

## TODO

- [ ] Versioning.
- [ ] Publish to [deno.land/x](https://deno.land/x).
- [ ] Document the Stampy JSON schema (which is pretty much explained fully in
      this README).
- [ ] Options for how to handle bad input.
