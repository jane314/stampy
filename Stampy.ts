type ITemplateElt = { name: string } | string;

function Stampy(
  template: ITemplateElt[],
  inputs: { [key: string]: string },
): string {
  let output = "";
  for (let elt of template) {
    if (typeof elt === "string") {
      output += elt;
    } else {
      output += inputs[elt.name];
    }
  }
  return output;
}

export { Stampy };
